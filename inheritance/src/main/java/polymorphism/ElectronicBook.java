package polymorphism;

public class ElectronicBook extends Book{
    private int numberBytes;

    public ElectronicBook(String name, String author, int numberBytes){
        super(name, author);
        this.numberBytes = numberBytes;
    }
    
    public int getNumberBytes(){
        return this.numberBytes;
    }

    public String toString(){
        String fromBase = super.toString();
        return fromBase + "Number of bytes: " + this.numberBytes;
    }
}
