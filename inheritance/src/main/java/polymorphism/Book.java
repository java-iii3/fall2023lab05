package polymorphism;

public class Book {
    protected String title;
    private String author;

    public Book(String title, String author){
        this.title = title;
        this.author = author;
    }

    public String getTitle(){
        return this.title;
    }

    public String author(){
        return this.author;
    }
    
    public String toString(){
        return "Author: " + this.author + " Book: " + this.title;
    }
}
