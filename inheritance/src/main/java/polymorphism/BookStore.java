package polymorphism;

public class BookStore {
    public static void main(String[] args){
        
        Book[] pileOfBooks = new Book[5];
        
        pileOfBooks[0] = new Book("Crime and Punishment", "Fyodor Dostoevsky");
        pileOfBooks[1] = new ElectronicBook("Anna Karenina", "Leo Tolstoy", 345);
        pileOfBooks[2] = new Book("Catcher in the Rye", "J.D. Salinger");
        pileOfBooks[3] = new ElectronicBook("The Great Gatsby", "F. Scott Fitzgerald", 678);
        pileOfBooks[4] = new ElectronicBook("No Longer Human", "Osamu Dazai", 1000);

        //pileOfBooks[0].getNumberOfBytes();
        //pileOfBooks[1].getNumberOfBytes();
        //ElectronicBook a = pileOfBooks[1];
        ElectronicBook b = (ElectronicBook)pileOfBooks[0];
        System.out.println(b.getNumberOfBytes());

        /*for(Book book : pileOfBooks){
            System.out.println(book);
        }*/
    }
}
